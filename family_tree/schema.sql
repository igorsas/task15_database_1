-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: family_tree
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `familiar_jewel`
--

DROP TABLE IF EXISTS `familiar_jewel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familiar_jewel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `max_cost` double NOT NULL,
  `min_cost` double NOT NULL,
  `oriented_cost` double GENERATED ALWAYS AS ((sin(`min_cost`) + cos(`max_cost`))) STORED,
  `code` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familiar_jewel`
--

LOCK TABLES `familiar_jewel` WRITE;
/*!40000 ALTER TABLE `familiar_jewel` DISABLE KEYS */;
INSERT INTO `familiar_jewel` (`id`, `name`, `max_cost`, `min_cost`, `code`) VALUES (1,'\'quun boucher\'',2000,500,1),(2,'\'bracelet Lisa\'',40000,5000,2);
/*!40000 ALTER TABLE `familiar_jewel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familiar_tree`
--

DROP TABLE IF EXISTS `familiar_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familiar_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `birthday` date NOT NULL,
  `date_of_death` date DEFAULT NULL,
  `birthplace` varchar(45) NOT NULL,
  `place_of_death` varchar(45) DEFAULT NULL,
  `credit_card` int(11) DEFAULT NULL,
  `familiar_tree_id` int(11) NOT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `gender_id` int(11) NOT NULL,
  `name` varchar(45) GENERATED ALWAYS AS (concat(`first_name`,' ',`surname`)) VIRTUAL,
  PRIMARY KEY (`id`),
  KEY `fk_familiar_tree_familiar_tree_idx` (`familiar_tree_id`),
  KEY `fk_familiar_tree_partner1_idx` (`partner_id`),
  KEY `fk_familiar_tree_gender1_idx` (`gender_id`),
  CONSTRAINT `fk_familiar_tree_familiar_tree` FOREIGN KEY (`familiar_tree_id`) REFERENCES `familiar_tree` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_familiar_tree_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_familiar_tree_partner1` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familiar_tree`
--

LOCK TABLES `familiar_tree` WRITE;
/*!40000 ALTER TABLE `familiar_tree` DISABLE KEYS */;
INSERT INTO `familiar_tree` (`id`, `first_name`, `surname`, `birthday`, `date_of_death`, `birthplace`, `place_of_death`, `credit_card`, `familiar_tree_id`, `partner_id`, `gender_id`) VALUES (1,'Lui','Boucher','1945-03-18',NULL,'Paris',NULL,11223344,1,1,1),(2,'Thromas','Boucher','1974-01-01',NULL,'Paris',NULL,11223345,1,2,1),(3,'Marie','Boucher','1976-09-08',NULL,'Paris',NULL,11223346,1,3,2),(4,'John','Boucher','1999-11-06',NULL,'London',NULL,11223347,2,NULL,1),(5,'Emma','Boucher','1996-05-12',NULL,'London',NULL,11223348,2,4,2),(6,'Kathy','Krchmar','1994-04-27',NULL,'Prague',NULL,11223349,3,5,2),(7,'Lui','Krchmar','1992-03-11',NULL,'Prague',NULL,11223350,3,NULL,1),(8,'Lisa','Hewnish','2018-01-05',NULL,'New York',NULL,11223351,5,NULL,2),(9,'Dorian','Harthry','2019-02-20',NULL,'Munchen',NULL,11223352,6,NULL,1);
/*!40000 ALTER TABLE `familiar_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familiar_tree_has_familiar_jewel`
--

DROP TABLE IF EXISTS `familiar_tree_has_familiar_jewel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familiar_tree_has_familiar_jewel` (
  `familiar_tree_id` int(11) NOT NULL,
  `familiar_jewel_id` int(11) NOT NULL,
  PRIMARY KEY (`familiar_tree_id`,`familiar_jewel_id`),
  KEY `fk_familiar_tree_has_familiar_jewel_familiar_jewel1_idx` (`familiar_jewel_id`),
  KEY `fk_familiar_tree_has_familiar_jewel_familiar_tree1_idx` (`familiar_tree_id`),
  CONSTRAINT `fk_familiar_tree_has_familiar_jewel_familiar_jewel1` FOREIGN KEY (`familiar_jewel_id`) REFERENCES `familiar_jewel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_familiar_tree_has_familiar_jewel_familiar_tree1` FOREIGN KEY (`familiar_tree_id`) REFERENCES `familiar_tree` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familiar_tree_has_familiar_jewel`
--

LOCK TABLES `familiar_tree_has_familiar_jewel` WRITE;
/*!40000 ALTER TABLE `familiar_tree_has_familiar_jewel` DISABLE KEYS */;
INSERT INTO `familiar_tree_has_familiar_jewel` VALUES (1,1),(2,1),(3,2);
/*!40000 ALTER TABLE `familiar_tree_has_familiar_jewel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,1),(2,0);
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `birthday` date NOT NULL,
  `date_of_death` date DEFAULT NULL,
  `birthplace` varchar(45) NOT NULL,
  `place_of_death` varchar(45) DEFAULT NULL,
  `date_of_wedding` date DEFAULT NULL,
  `info` varchar(45) GENERATED ALWAYS AS (concat(`first_name`,' ',`surname`,' народ. ',dayofyear(`birthday`),' дня ',year(`birthday`),'р. від Різдва Христового')) VIRTUAL,
  `gender_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_partner_gender1_idx` (`gender_id`),
  CONSTRAINT `fk_partner_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `first_name`, `surname`, `birthday`, `date_of_death`, `birthplace`, `place_of_death`, `date_of_wedding`, `gender_id`) VALUES (1,'Lisa','Haus','1950-11-28','2015-06-12','Lublin','Paris','1973-02-16',2),(2,'Mary','Poppins','1975-04-11',NULL,'London',NULL,'1991-02-19',2),(3,'Petr','Krchmar','1973-06-02',NULL,'Prague',NULL,'1993-02-09',1),(4,'Andrew','Hewnish','1996-05-13',NULL,'New York',NULL,'2016-08-14',1),(5,'Tom','Harthry','1994-11-09',NULL,'Munchen',NULL,'2018-07-13',1);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-02 17:38:01
