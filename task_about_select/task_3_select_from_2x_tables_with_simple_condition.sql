-- Select from 2x table with simple condition

#1
select 
product.maker, 
product.type, 
pc.speed, 
pc.hd
from product
join pc
on product.model = pc.model
where pc.hd <= 8;

#2
select product.maker
from product
join pc
on product.model = pc.model
where product.maker 
not in
(select product.maker
from product
join pc
on product.model = pc.model
where pc.speed < 600
group by product.maker)
group by product.maker;

#3
select product.maker
from product
join pc
on product.model = pc.model
where product.maker 
not in
(select product.maker
from product
join laptop
on product.model = laptop.model
where laptop.speed > 500
group by product.maker)
group by product.maker;

#4
select 
l1.model as model1,
l2.model as model2
from laptop as l1
join laptop as l2
on (l1.ram = l2.ram and l1.hd = l2.hd)
where l1.code != l2.code
group by model1;

#5
select country
from classes
where `type` = 'bb'
and country 
in
(select country
from classes
where `type` = 'bc'
group by country)
group by country;

#6
select 
pc.model, 
product.maker
from pc
join product
on pc.model = product.model
where pc.price < 600
group by model;

#7
select 
printer.model, 
product.maker
from printer
join product
on printer.model = product.model
where printer.price < 600
group by model;

#8
select 
pc.model, 
product.maker, 
pc.price
from pc
join product
on pc.model = product.model
order by price;

#9
-- the same like 8

#10
select 
product.maker,
product.type,
laptop.model,
laptop.speed
from laptop
join product
on laptop.model = product.model
where laptop.speed <= 600;

#11
select 
ships.name, 
ships.class, 
ships.launched, 
classes.displacement
from ships
join classes
on ships.class = classes.class;

#12
select 
ships.name, 
ships.class, 
ships.launched, 
battles.name, 
battles.date
from outcomes
join ships
on ships.name = outcomes.ship
join battles
on battles.name = outcomes.battle
where outcomes.result != 'sunk';

#13
select 
ships.name, 
ships.class, 
ships.launched, 
classes.country
from ships
join classes
on ships.class = classes.class;

#14
select 
trip.plane, 
company.name
from trip
join company
on trip.ID_comp = company.ID_comp
where trip.plane = 'Boeing'
group by company.name;

#15
select 
passenger.name, 
pass_in_trip.date
from pass_in_trip
join passenger
on pass_in_trip.ID_psg = passenger.ID_psg;