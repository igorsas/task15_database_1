-- Select from the table with simple condition(regex)

#1
select * from pc
where model rlike '1+.*1+';

#2
select * from outcome
where month(`date`) = 3;

#3
select * from outcome_o
where day(`date`) = 14;

#4
select `name` from ships
where name rlike '^W+.*n$';

#5
select `name` from ships
where name rlike 'e+.*e+';

#6
select `name` from ships
where name rlike '[b-z]$|[0-9]$';

#7
select `name` from battles
where `name` rlike '[[:blank:]]+.*[a-b,d-z]$';

#8
select * from trip
where time(`time_out`) >= '12:00'
and time(`time_out`) <= '17:00';

#9
select * from trip
where time(`time_in`) >= '17:00'
and time(`time_in`) <= '23:00';

#10
select `date` from pass_in_trip
where place rlike '^1';

#11
select `date` from pass_in_trip
where place rlike 'c$';

#12
select
substring(name, position(' ' in name), char_length(name)) as surname
from passenger
where name rlike '[[:blank:]]+C+';

#13
select
substring(name, position(' ' in name), char_length(name)) as surname
from passenger 
where name not rlike '[[:blank:],-]+J+';