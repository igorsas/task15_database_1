-- Select from the table with simple condition

#1
select maker, type 
from product
order by maker;

#2
select model, ram, screen, price 
from laptop
where price > 1000
order by ram asc, price desc;

#3
select * from printer
order by price desc;

#4
select model, speed, hd, cd, price
from pc
where (cd = '12x' 
or cd = '24x') 
and price < 600
order by speed desc;

#5
select name, class
from ships
order by name;

#6
select * from pc
where speed >= 500
and price < 800
order by price desc;

#7
select * from printer
where type != 'Matrix'
and price < 300
order by type desc;

#8
select model, speed
from pc
where price >= 400 
and price <= 600
order by hd;

#9
select model, speed, hd
from pc
where hd = 10 
or hd = 20
order by speed;

#10
select model, speed, hd, price
from laptop
where screen >= 12
order by price desc;

#11
select model, type, price
from printer
where price < 300
order by type desc;

#12
select model, ram, price
from laptop
where ram = 64
order by screen;

#13
select model, ram, price
from pc
where ram > 64
order by hd;

#14
select model, speed, price
from pc
where speed >= 500 
and speed <= 750
order by hd desc;

#15
select * from outcome_o
where `out` > 2000
order by date desc;

#16
select * from income_o
where inc >= 5000
and inc <= 10000
order by inc;

#17
select * from income
where point = 1
order by inc;

#18
select * from outcome
where point = 2
order by `out`;

#19
select * from classes
where country = 'Japan'
order by type desc;

#20
select name, launched
from ships
where launched >= 1920
and launched <= 1942
order by launched desc;

#21
select * from outcomes
where battle = 'Guadalcanal'
and result != 'sunk'
order by ship desc;

#22
select * from outcomes
where result = 'sunk'
order by ship desc;

#23
select ships.name, classes.class, classes.displacement
from ships
join classes
on ships.class = classes.class
where classes.displacement >= 40
order by classes.type;

#24
select trip_no, town_from, town_to
from trip
where town_from = 'London'
or town_to = 'London'
order by time_out;

#25
select trip_no, town_from, town_to
from trip
where plane = 'TU-134'
order by time_out desc;

#26
select trip_no, town_from, town_to
from trip
where plane = 'IL-86'
order by plane;

#27
select trip_no, town_from, town_to
from trip
where town_from != 'Rostov'
and town_to != 'Rostov'
order by plane;